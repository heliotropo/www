---
title: "Manifesto"
date: 2023-03-22T19:25:59+01:00
draft: false
---

Molti di noi, quasi tutti, oramai lo portano in tasca, nella borsa,
lo tengono sul tavolo, in automobile, sul comodino. E' quasi sempre
acceso; alcuni lo spengono forse solo di notte, ai concerti. La gente
lo adopera per la strada. Di solito si chiama telefonino o cellulare,
telefono portatile.

 Oggi però viene adoperato per molte più funzioni
che parlarci e rispondere. Racchiude una parte rilevante della nostra
vita: soprattutto della vita di relazioni con gli altri, nel bene e nel male.
Oltre alle parole ed ai suoni offre immagini, molte, troppe.

Prende energia dalla batteria senza la quale non funziona e va spesso
ricaricata attaccandosi alla rete elettrica. Nella nostra ‘grande’ epoca
si sprecano i discorsi sull’energia, sui mutamenti climatici conseguenti
ed i necessari risparmi energetici. Dopo averne negato per anni la
possibilità, persino anche le industrie peggio restie si sono poi messe
a produrre automobili elettriche in varie forme. Oggi per queste esiste
un mercato ed i governi stanziano incentivi, anche se non sufficienti.

Eppure non esiste ancora un modello di telefono equivalente che realizzi il risparmio di energia elettrica. 

Allora ci proponiamo di costruirlo subito, il prima possibile. 
Esso diventerà inevitabile come un autoelettrica diffusa.  
  

## Aspetti tecnici  
  
Il progetto presenta ostacoli che andranno superati.  
  
1. Installare sul retro del telefono e sulle custodie eventuali la quantità di cellule fotovoltaiche necessaria per ricaricare la batteria attraverso la luce dell’ambiente, sia al chiuso che all’aperto.
2. Limitare gli usi del telefono a quelli essenziali: le parole, gli scritti.
Ridurre il più possibile le immagini che sono energivore. Esse sono quasi
sempre pubblicità, sia esplicita che occulta. Cancellare allora tutte le
funzioni che non siano utili e richieste dall’utente singolo. Ciascuno ha
le proprie legate alla professione, alle abitudini alla vita che conduce.


Heliotropo inventerà un telefono portatile, quasi sempre carico e
capace di un buon risparmio energetico.

Il prototipo funzionante sarà costruito e brevettato. 